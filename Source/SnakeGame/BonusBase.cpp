// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBase.h"

// Sets default values
ABonusBase::ABonusBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABonusBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		BonusAction(Snake);
		BonusActivated.Broadcast();
	}
}
void ABonusBase::BonusAction(ASnakeBase* Snake)
{

}

void ABonusBase::AutoDestroy()
{
	BonusActivated.Broadcast();
}

void ABonusBase::UpdateTimeToDie(float TimeToDie)
{
	FTimerHandle SpawnTimer;
	GetWorld()->GetTimerManager().SetTimer(SpawnTimer, this, &ABonusBase::AutoDestroy, TimeToDie - 0.01f, false);
	this->SetLifeSpan(TimeToDie);
}


