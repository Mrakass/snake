// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeBase.h"
#include "BonusBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMyDelegate);

UCLASS()
class SNAKEGAME_API ABonusBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	ABonusBase();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override; 

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION()
		virtual void BonusAction(ASnakeBase* Snake);

	UFUNCTION()
		void AutoDestroy();

	void UpdateTimeToDie(float TimeToDie);

	FMyDelegate BonusActivated;
};
