// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusSpeed.h"

void ABonusSpeed::BonusAction(ASnakeBase* Snake)
{
	Super::BonusAction(Snake);

	Snake->SetActorTickInterval(Snake->MovementSpeed -= BonusPower);

	Destroy();
}
