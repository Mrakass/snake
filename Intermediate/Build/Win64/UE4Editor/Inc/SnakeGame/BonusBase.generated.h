// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeBase;
#ifdef SNAKEGAME_BonusBase_generated_h
#error "BonusBase.generated.h already included, missing '#pragma once' in BonusBase.h"
#endif
#define SNAKEGAME_BonusBase_generated_h

#define SnakeGame_Source_SnakeGame_BonusBase_h_11_DELEGATE \
static inline void FMyDelegate_DelegateWrapper(const FMulticastScriptDelegate& MyDelegate) \
{ \
	MyDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define SnakeGame_Source_SnakeGame_BonusBase_h_16_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_BonusBase_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAutoDestroy); \
	DECLARE_FUNCTION(execBonusAction);


#define SnakeGame_Source_SnakeGame_BonusBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAutoDestroy); \
	DECLARE_FUNCTION(execBonusAction);


#define SnakeGame_Source_SnakeGame_BonusBase_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABonusBase(); \
	friend struct Z_Construct_UClass_ABonusBase_Statics; \
public: \
	DECLARE_CLASS(ABonusBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ABonusBase) \
	virtual UObject* _getUObject() const override { return const_cast<ABonusBase*>(this); }


#define SnakeGame_Source_SnakeGame_BonusBase_h_16_INCLASS \
private: \
	static void StaticRegisterNativesABonusBase(); \
	friend struct Z_Construct_UClass_ABonusBase_Statics; \
public: \
	DECLARE_CLASS(ABonusBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ABonusBase) \
	virtual UObject* _getUObject() const override { return const_cast<ABonusBase*>(this); }


#define SnakeGame_Source_SnakeGame_BonusBase_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABonusBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABonusBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusBase(ABonusBase&&); \
	NO_API ABonusBase(const ABonusBase&); \
public:


#define SnakeGame_Source_SnakeGame_BonusBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABonusBase(ABonusBase&&); \
	NO_API ABonusBase(const ABonusBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABonusBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABonusBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABonusBase)


#define SnakeGame_Source_SnakeGame_BonusBase_h_16_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_BonusBase_h_13_PROLOG
#define SnakeGame_Source_SnakeGame_BonusBase_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_BonusBase_h_16_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_BonusBase_h_16_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_BonusBase_h_16_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_BonusBase_h_16_INCLASS \
	SnakeGame_Source_SnakeGame_BonusBase_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_BonusBase_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_BonusBase_h_16_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_BonusBase_h_16_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_BonusBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_BonusBase_h_16_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_BonusBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ABonusBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_BonusBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
