// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/BonusSpeed.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBonusSpeed() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusSpeed_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusSpeed();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusBase();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
// End Cross Module References
	void ABonusSpeed::StaticRegisterNativesABonusSpeed()
	{
	}
	UClass* Z_Construct_UClass_ABonusSpeed_NoRegister()
	{
		return ABonusSpeed::StaticClass();
	}
	struct Z_Construct_UClass_ABonusSpeed_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BonusPower_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BonusPower;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABonusSpeed_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ABonusBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusSpeed_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BonusSpeed.h" },
		{ "ModuleRelativePath", "BonusSpeed.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusSpeed_Statics::NewProp_BonusPower_MetaData[] = {
		{ "Category", "BonusSpeed" },
		{ "ModuleRelativePath", "BonusSpeed.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABonusSpeed_Statics::NewProp_BonusPower = { "BonusPower", nullptr, (EPropertyFlags)0x0040000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABonusSpeed, BonusPower), METADATA_PARAMS(Z_Construct_UClass_ABonusSpeed_Statics::NewProp_BonusPower_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusSpeed_Statics::NewProp_BonusPower_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABonusSpeed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABonusSpeed_Statics::NewProp_BonusPower,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABonusSpeed_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABonusSpeed>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABonusSpeed_Statics::ClassParams = {
		&ABonusSpeed::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ABonusSpeed_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ABonusSpeed_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABonusSpeed_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusSpeed_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABonusSpeed()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABonusSpeed_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABonusSpeed, 3222413067);
	template<> SNAKEGAME_API UClass* StaticClass<ABonusSpeed>()
	{
		return ABonusSpeed::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABonusSpeed(Z_Construct_UClass_ABonusSpeed, &ABonusSpeed::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ABonusSpeed"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABonusSpeed);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
