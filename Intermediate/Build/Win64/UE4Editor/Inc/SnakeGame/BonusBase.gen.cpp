// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/BonusBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBonusBase() {}
// Cross Module References
	SNAKEGAME_API UFunction* Z_Construct_UDelegateFunction_SnakeGame_MyDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusBase_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABonusBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	SNAKEGAME_API UClass* Z_Construct_UClass_ASnakeBase_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_SnakeGame_MyDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SnakeGame_MyDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BonusBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_SnakeGame_MyDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_SnakeGame, nullptr, "MyDelegate__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_SnakeGame_MyDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_SnakeGame_MyDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_SnakeGame_MyDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_SnakeGame_MyDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(ABonusBase::execAutoDestroy)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AutoDestroy();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABonusBase::execBonusAction)
	{
		P_GET_OBJECT(ASnakeBase,Z_Param_Snake);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BonusAction(Z_Param_Snake);
		P_NATIVE_END;
	}
	void ABonusBase::StaticRegisterNativesABonusBase()
	{
		UClass* Class = ABonusBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AutoDestroy", &ABonusBase::execAutoDestroy },
			{ "BonusAction", &ABonusBase::execBonusAction },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BonusBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABonusBase, nullptr, "AutoDestroy", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABonusBase_AutoDestroy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABonusBase_AutoDestroy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABonusBase_BonusAction_Statics
	{
		struct BonusBase_eventBonusAction_Parms
		{
			ASnakeBase* Snake;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Snake;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABonusBase_BonusAction_Statics::NewProp_Snake = { "Snake", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BonusBase_eventBonusAction_Parms, Snake), Z_Construct_UClass_ASnakeBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABonusBase_BonusAction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABonusBase_BonusAction_Statics::NewProp_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABonusBase_BonusAction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BonusBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABonusBase_BonusAction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABonusBase, nullptr, "BonusAction", nullptr, nullptr, sizeof(BonusBase_eventBonusAction_Parms), Z_Construct_UFunction_ABonusBase_BonusAction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonusBase_BonusAction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABonusBase_BonusAction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABonusBase_BonusAction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABonusBase_BonusAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABonusBase_BonusAction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABonusBase_NoRegister()
	{
		return ABonusBase::StaticClass();
	}
	struct Z_Construct_UClass_ABonusBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABonusBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABonusBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABonusBase_AutoDestroy, "AutoDestroy" }, // 2803093958
		{ &Z_Construct_UFunction_ABonusBase_BonusAction, "BonusAction" }, // 1943887796
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABonusBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BonusBase.h" },
		{ "ModuleRelativePath", "BonusBase.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ABonusBase_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ABonusBase, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABonusBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABonusBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABonusBase_Statics::ClassParams = {
		&ABonusBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABonusBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABonusBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABonusBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABonusBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABonusBase, 1399963378);
	template<> SNAKEGAME_API UClass* StaticClass<ABonusBase>()
	{
		return ABonusBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABonusBase(Z_Construct_UClass_ABonusBase, &ABonusBase::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ABonusBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABonusBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
